module gitlab.com/golang-team-10/golang-mts-teta

go 1.17

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/urfave/cli v1.22.5
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	go.uber.org/zap v1.19.1
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
