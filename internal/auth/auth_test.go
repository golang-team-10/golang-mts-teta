package auth_test

import (
	"crypto/sha256"
	"encoding/base64"
	"strings"
	"testing"

	"gitlab.com/golang-team-10/golang-mts-teta/internal/auth"
)

var _salt_ = "SHtseXFywxVN"

func TestEncodePass(t *testing.T) {
	pass := "admin"
	t.Logf("salt=%s\n", base64.StdEncoding.EncodeToString([]byte(_salt_)))

	hash := sha256.New()
	hash.Write([]byte(_salt_))
	b := hash.Sum([]byte(pass))
	t.Logf("hash=%s\n", base64.StdEncoding.EncodeToString(b))
}

func TestDecodeSalt(t *testing.T) {
	salt, err := base64.StdEncoding.DecodeString("U0h0c2VYRnl3eFZO")
	if err != nil {
		t.Fatal(err)
	} else if string(salt) != _salt_ {
		t.Fatalf("want %s, received %s", _salt_, string(salt))
	}
}

func TestHash(t *testing.T) {
	r := strings.NewReader(`
	#user:salt:password
	admin:U0h0c2VYRnl3eFZO:YWRtaW7Ci3aYXcAmVjZb3egDurWm/pxW6dMyItDd8SPaKDOY9g==`)
	a, err := auth.NewAuthData(r)
	if err != nil {
		t.Fatal(err)
	} else if err = a.Validate("admin", "admin"); err != nil {
		t.Fatal(err)
	}
}
