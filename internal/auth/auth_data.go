package auth

import (
	"bufio"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"strings"
)

type row struct {
	user string
	salt string
	hash string
}

type AuthData struct {
	data map[string]row
}

func NewAuthData(r io.Reader) (*AuthData, error) {
	var n int
	m := make(map[string]row)
	b := bufio.NewScanner(r)
	for b.Scan() {
		n++
		t := strings.TrimSpace(b.Text())
		i := strings.IndexRune(t, '#')
		if i >= 0 {
			t = t[:i]
		}
		if len(t) == 0 {
			continue
		}
		a := strings.SplitN(t, ":", 3)
		if len(a) != 3 {
			return nil, fmt.Errorf("error at line %d, items count is %d", n, len(a))
		}
		user := a[0]
		salt := a[1]
		hash := a[2]
		m[user] = row{
			user: user,
			salt: salt,
			hash: hash,
		}
	}
	return &AuthData{data: m}, nil
}

func (a *AuthData) Validate(username, password string) error {
	if v, ok := a.data[username]; ok {
		hash := sha256.New()
		salt, err := base64.StdEncoding.DecodeString(v.salt)
		if err != nil {
			return err
		}
		hash.Write(salt)
		s := base64.StdEncoding.EncodeToString(hash.Sum([]byte(password)))
		if s != v.hash {
			return fmt.Errorf("invalid password for %s", username)
		}
		return nil
	}
	return fmt.Errorf("auth data for %s not found", username)
}
