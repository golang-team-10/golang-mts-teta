package profile

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/urfave/cli"

	"gitlab.com/golang-team-10/golang-mts-teta/cmd/base"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/jwt"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/receivers"
)

var Cmd = cli.Command{
	Name:        "profile",
	Action:      run,
	Usage:       "main profile service",
	Description: "business logic app",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:     "auth-addr",
			Usage:    "auth addr",
			EnvVar:   "AUTH_ADDR",
			Required: true,
		},
	},
}

func run(ctx *cli.Context) error {
	s := http.Server{
		Addr:         ctx.String("addr"),
		Handler:      Router(),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}
	return s.ListenAndServe()
}

func Router() *mux.Router {

	r := mux.NewRouter()
	r.Path("/receivers/{user_id}/items").
		Methods(http.MethodGet).
		HandlerFunc(handleItems)
	r.Path("/receivers/{user_id}/items/{item_id:[0-9]+}").
		Methods(http.MethodGet).
		HandlerFunc(handleItem)
	r.Path("/receivers/{user_id}/items").
		Methods(http.MethodPost).
		Queries("name", "{name}").
		HandlerFunc(handleCreate)
	r.Path("/receivers/{user_id}/items/{item_id}").
		Methods(http.MethodPatch).
		Queries("name", "{name}").
		HandlerFunc(handleUpdate)
	r.Path("/receivers/{user_id}/items/{item_id:[0-9]+}").
		Methods(http.MethodDelete).
		HandlerFunc(handleDelete)

	p := mux.NewRouter()
	p.Use(jwt.Middleware)
	p.Path("/i").HandlerFunc(handle_i)
	p.NotFoundHandler = r

	return base.Router(p)
}

func handle_i(w http.ResponseWriter, r *http.Request) {
	resp := struct {
		Username string `json:"username"`
	}{
		Username: jwt.GetUsername(r),
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func handleItem(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	item, err := strconv.Atoi(vars["item_id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err)
		return
	}
	items, err := receivers.GetAll(r.Context(), vars["user_id"])
	if err != nil {
		w.WriteHeader(http.StatusRequestTimeout)
		fmt.Fprint(w, err)
		return
	} else if item > len(items) {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, items[item-1].Name)
}

func handleItems(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	items, err := receivers.GetAll(r.Context(), vars["user_id"])
	if err != nil {
		w.WriteHeader(http.StatusRequestTimeout)
		fmt.Fprint(w, err)
		return
	}
	var out []string
	for _, r := range items {
		out = append(out, r.Name)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(out)
}

func handleCreate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := vars["user_id"]
	item, err := receivers.Create(r.Context(), user, vars["name"])
	if err != nil {
		w.WriteHeader(http.StatusRequestTimeout)
		fmt.Fprint(w, err)
		return
	}
	w.Header().Add("Location", fmt.Sprintf("/receivers/%s/items/%d", user, item))
	w.WriteHeader(http.StatusCreated)
	fmt.Fprint(w, item)
}

func handleUpdate(w http.ResponseWriter, r *http.Request) {
	// TODO
	vars := mux.Vars(r)
	item, err := strconv.Atoi(vars["item_id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err)
		return
	}
	receivers.Update(vars["user_id"], item, vars["name"])
	w.WriteHeader(http.StatusAccepted)
}

func handleDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	item, err := strconv.Atoi(vars["item_id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err)
		return
	}
	receivers.Delete(vars["user_id"], item)
	w.WriteHeader(http.StatusOK)
}
