
.PHONY: up down docker

TAG     ?= $(shell git rev-parse --short HEAD)
VERSION ?= $(shell git describe --tags --exact-match 2>/dev/null || git rev-parse --abbrev-ref HEAD)

LDFLAGS := -X "main.Version=$(VERSION)" -X "main.Tag=$(TAG)" $(LDFLAGS)

local: main.go
	go run -v -ldflags '-s -w $(LDFLAGS)' $^ --net-pprof-addr :3000 --auth-data passwords.txt

docker: IMAGE = golang-mts-teta:$(VERSION)
docker: main.go
	docker build -f docker/Dockerfile -t golang-mts-teta:$(VERSION) .
	IMAGE=$(IMAGE) docker-compose -f docker/docker-compose.yaml up -d
	open http://localhost:8080/
	docker ps

test:
	go clean -testcache
	go test -v ./...

cover:
	go test ./... -coverprofile cover.out
	go tool cover -html=cover.out

up: IMAGE = registry.gitlab.com/golang-team-10/golang-mts-teta:${VERSION}
up:
	docker pull $(IMAGE)
	IMAGE=$(IMAGE) docker-compose -f docker/docker-compose.yaml up -d
	open http://localhost:8080/
	docker ps

down:
	docker-compose -f docker/docker-compose.yaml down
