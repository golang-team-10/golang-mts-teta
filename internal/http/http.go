package test

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/go-yaml/yaml"
)

type AllTests struct {
	Tests []MyTest
}

type MyTest struct {
	Url                string  `yaml:"url"`
	Name               string  `yaml:"name"`
	Method             string  `yaml:"method"`
	Username           string  `yaml:"username"`
	Password           string  `yaml:"password"`
	StatusCode         int     `yaml:"status"`
	InputAccessToken   *string `yaml:"cli_access"`
	InputRefreshToken  *string `yaml:"cli_refresh"`
	OutputAccessToken  *string `yaml:"srv_access"`
	OutputRefreshToken *string `yaml:"srv_refresh"`
	Body               string  `yaml:"body"`
	Resp               string  `yaml:"resp"`
	Only               bool    `yaml:"only"`
}

const (
	kAccessToken  = "access-token"
	kRefreshToken = "refresh-token"
)

var ErrRedirect = errors.New("redirect")

func RunTest(t *testing.T, name string, ts *httptest.Server, file string) {
	f, err := os.Open(file)
	if err != nil {
		t.Fatal(err)
	}
	var all AllTests
	if err := yaml.NewDecoder(f).Decode(&all); err != nil {
		t.Fatal(err)
	}
	runAll := true
	for _, tt := range all.Tests {
		if tt.Only {
			t.Run(name+"/"+tt.Name+tt.Url, tt.makeTestFunc(ts))
			runAll = false
		}
	}
	if runAll {
		for _, tt := range all.Tests {
			t.Run(name+"/"+tt.Name+tt.Url, tt.makeTestFunc(ts))
		}
	}
}

func (tt MyTest) makeTestFunc(ts *httptest.Server) func(t *testing.T) {
	return func(t *testing.T) {
		method := map[string]string{
			"get":    http.MethodGet,
			"put":    http.MethodPut,
			"post":   http.MethodPost,
			"patch":  http.MethodPatch,
			"delete": http.MethodDelete,
		}[tt.Method]
		if method == "" {
			method = http.MethodPost
		}
		req, err := http.NewRequest(method, ts.URL+tt.Url, strings.NewReader(tt.Body))
		if err != nil {
			t.Fatal(err)
		}
		if tt.Username != "" || tt.Password != "" {
			req.SetBasicAuth(tt.Username, tt.Password)
		}
		if tt.InputAccessToken != nil {
			req.AddCookie(&http.Cookie{
				Name:  kAccessToken,
				Value: *tt.InputAccessToken,
			})
		}
		if tt.InputRefreshToken != nil {
			req.AddCookie(&http.Cookie{
				Name:  kRefreshToken,
				Value: *tt.InputRefreshToken,
			})
		}
		res, err := ts.Client().Do(req)
		if err != nil && !errors.Is(err, ErrRedirect) {
			t.Fatal(err)
		}
		defer res.Body.Close()
		body, err := io.ReadAll(res.Body)
		if err != nil {
			t.Fatal(err)
		} else if !(res.StatusCode == tt.StatusCode || tt.StatusCode == 0 && res.StatusCode == http.StatusOK) {
			t.Fatalf("want status %d, received: %d (%s) for url %s body [%s]", tt.StatusCode, res.StatusCode, res.Status, tt.Url, string(body))
		} else if err := compareToken(res, kAccessToken, tt.OutputAccessToken); err != nil {
			t.Fatal(err)
		} else if err := compareToken(res, kRefreshToken, tt.OutputRefreshToken); err != nil {
			t.Fatal(err)
		} else if tt.Resp != "" {
			s1, s2 := strings.TrimSpace(tt.Resp), strings.TrimSpace(string(body))
			if s1 != s2 {
				t.Fatalf("want body %s, received %s", s1, s2)
			}
		}
	}
}

func compareToken(res *http.Response, cookieName string, wantValue *string) error {
	var cookie *string
	for _, c := range res.Cookies() {
		if c.Name == cookieName {
			cookie = &c.Value
			break
		}
	}
	want_cookie := wantValue != nil && *wantValue != ""
	want_empty := wantValue != nil && *wantValue == ""
	cookie_empty := cookie == nil || *cookie == ""

	// compare cookie
	if want_cookie && !cookie_empty && *wantValue != *cookie {
		return fmt.Errorf("want %s = %s, received %s", cookieName, *wantValue, *cookie)
	}
	// clear cookie
	if want_empty && !cookie_empty {
		return fmt.Errorf("want %s empty, received %s", cookieName, *cookie)
	}
	// want cookie but no cookie
	if want_cookie && cookie_empty {
		return fmt.Errorf("want %s = %s, received empty", cookieName, *wantValue)
	}
	return nil
}
