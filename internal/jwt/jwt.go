package jwt

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/setting"
)

const (
	kAccessToken  = "access-token"
	kRefreshToken = "refresh-token"

	kAccessTokenDuration  = time.Minute
	kRefreshTokenDuration = time.Hour
)

type contextKey int

const (
	usernameKey contextKey = iota
)

func GetUsername(r *http.Request) string {
	return r.Context().Value(usernameKey).(string)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r2, _, err := validateToken(r, kAccessToken); err == nil {
			next.ServeHTTP(w, r2)
			return
		}
		r2, username, err := validateToken(r, kRefreshToken)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if err := AddTokens(w, username); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			setting.Logger.Error(err)
			return
		}
		next.ServeHTTP(w, r2)
	})
}

func validateToken(r *http.Request, name string) (r2 *http.Request, username string, err error) {
	var cookie *http.Cookie
	cookie, err = r.Cookie(name)
	if err != nil {
		return
	}
	var claims jwt.StandardClaims
	if _, err = jwt.ParseWithClaims(cookie.Value, &claims, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v for cookie: %s",
				t.Header["alg"],
				cookie.Value)
		}
		return setting.HmacSecret, nil
	}); err != nil {
		return
	}
	username = claims.Subject
	r2 = r.WithContext(
		context.WithValue(
			r.Context(),
			usernameKey,
			username))
	return
}

func AddTokens(w http.ResponseWriter, username string) error {
	accessToken, err := makeToken(username, kAccessTokenDuration)
	if err != nil {
		return err
	}
	refreshToken, err := makeToken(username, kRefreshTokenDuration)
	if err != nil {
		return err
	}
	accessCookie := http.Cookie{
		Name:     kAccessToken,
		Value:    accessToken,
		MaxAge:   int(kAccessTokenDuration.Seconds()),
		Expires:  jwt.TimeFunc().Add(kAccessTokenDuration),
		HttpOnly: true,
	}
	refreshCookie := http.Cookie{
		Name:     kRefreshToken,
		Value:    refreshToken,
		MaxAge:   int(kRefreshTokenDuration.Seconds()),
		Expires:  jwt.TimeFunc().Add(kRefreshTokenDuration),
		HttpOnly: true,
	}
	http.SetCookie(w, &accessCookie)
	http.SetCookie(w, &refreshCookie)
	return nil
}

func ClearTokens(w http.ResponseWriter) {
	cookie1 := &http.Cookie{
		Name:     kAccessToken,
		Value:    "",
		MaxAge:   -1,
		Expires:  time.Now(),
		HttpOnly: true,
	}
	cookie2 := &http.Cookie{
		Name:     kRefreshToken,
		Value:    "",
		MaxAge:   -1,
		Expires:  time.Now(),
		HttpOnly: true,
	}
	http.SetCookie(w, cookie1)
	http.SetCookie(w, cookie2)
}

func makeToken(username string, duration time.Duration) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		ExpiresAt: jwt.TimeFunc().Add(duration).Unix(),
		Subject:   username,
	})
	return token.SignedString(setting.HmacSecret)
}
