
`make local` для локальной сборки и запуска

`make docker` для локальной сборки и запуска из под докера

`make up` & `make down` для docker pull с GitLab registry и запуска на локальном докере

сервис поднимается на [localhost:8080](http://localhost:8080/)

curl -v -H 'Authorization: Basic YWRtaW46YWRtaW4=' http://localhost:8080/login

Способ проверки:  
* `make up`  
* `http://localhost:8080/`  
* `http://localhost:8080/login`  
* спросит логин/пароль, вводим `admin/admin`  
* `http://localhost:8080/i`, покажет текущего пользователя  
* `http://localhost:8080/logout`  
* `http://localhost:8080/i`, ничего не покажет
