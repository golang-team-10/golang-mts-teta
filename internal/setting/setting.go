package setting

import "go.uber.org/zap"

var HmacSecret []byte
var Logger *zap.SugaredLogger

func LoggerSync() error {
	if Logger != nil {
		return Logger.Sync()
	}
	return nil
}
