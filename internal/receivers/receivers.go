package receivers

import "context"

type Receiver struct {
	Name string
}

var ch_ chan func()
var receivers_ map[string][]Receiver

func init() {
	ch_ = make(chan func(), 1)
	receivers_ = make(map[string][]Receiver)
	go func() {
		for f := range ch_ {
			f()
		}
	}()
}

func GetAll(ctx context.Context, user string) ([]Receiver, error) {
	c := make(chan []Receiver, 1)
	ch_ <- func() {
		var out []Receiver
		for _, item := range receivers_[user] {
			out = append(out, Receiver{
				Name: item.Name,
			})
		}
		c <- out
	}
	select {
	case out := <-c:
		return out, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func Create(ctx context.Context, user string, name string) (int, error) {
	c := make(chan int, 1)
	ch_ <- func() {
		items := append(receivers_[user], Receiver{
			Name: name,
		})
		receivers_[user] = items
		c <- len(items)
	}
	select {
	case n := <-c:
		return n, nil
	case <-ctx.Done():
		return 0, ctx.Err()
	}
}

func Update(user string, item int, name string) {
	ch_ <- func() {
		items := receivers_[user]
		if item > len(items) {
			return
		}
		n := items[:item-1]
		n = append(n, Receiver{
			Name: name,
		})
		n = append(n, items[item:]...)
		receivers_[user] = n
	}
}

func Delete(user string, item int) {
	ch_ <- func() {
		items := receivers_[user]
		if item > len(items) {
			return
		}
		receivers_[user] = append(items[:item-1], items[item:]...)
	}
}
