package auth

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/golang-team-10/golang-mts-teta/cmd/base"
	ad "gitlab.com/golang-team-10/golang-mts-teta/internal/auth"
	h2 "gitlab.com/golang-team-10/golang-mts-teta/internal/http"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/setting"
)

func init() {
	base.StaticDir = "../../static"
	setting.HmacSecret = []byte("123456")
	jwt.TimeFunc = func() time.Time {
		return time.Date(2021, 10, 8, 17, 2, 37, 0, time.UTC)
	}
}

func TestAuth(t *testing.T) {
	authData, _ := ad.NewAuthData(strings.NewReader(`
	#user:salt:password
	admin:U0h0c2VYRnl3eFZO:YWRtaW7Ci3aYXcAmVjZb3egDurWm/pxW6dMyItDd8SPaKDOY9g==`))
	ts := httptest.NewServer(Router(authData))
	ts.Client().CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return h2.ErrRedirect
	}
	h2.RunTest(t, "auth", ts, "auth_test.yaml")
}
