package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/urfave/cli"
	"go.uber.org/zap"

	"gitlab.com/golang-team-10/golang-mts-teta/cmd/auth"
	"gitlab.com/golang-team-10/golang-mts-teta/cmd/profile"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/setting"
)

var Tag = "tag"
var Version = "version"

func helloworld() string {
	return "Hello World!! I am " + Version + "@" + Tag
}

func healthcheck() string {
	return "Health OK!"
}

func livenesscheck() string {
	return "I am alive!!!"
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, helloworld())
	})

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, healthcheck())
	})

	http.HandleFunc("/liveness", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, livenesscheck())
	})

	app := cli.NewApp()
	app.Name = "golang-mts-teta"
	app.Usage = "mail sender"
	app.Description = "mail sender"
	app.Version = Version + ":" + Tag
	app.Commands = []cli.Command{
		auth.Cmd,
		profile.Cmd,
	}
	defaultFlags := []cli.Flag{
		cli.StringFlag{
			Name:   "addr",
			Value:  ":8080",
			Usage:  "server addr",
			EnvVar: "SERVER_ADDR",
		},
		cli.StringFlag{
			Name:  "net-pprof-addr",
			Usage: "net/pprof addr",
			Value: ":6000",
		},
		cli.BoolFlag{
			Name:  "debug",
			Usage: "debug",
		},
	}
	app.Flags = append(app.Flags, auth.Cmd.Flags...)
	app.Flags = append(app.Flags, defaultFlags...)
	app.Action = auth.Cmd.Action
	app.Before = beforeRun
	app.After = func(c *cli.Context) error {
		return setting.LoggerSync()
	}

	for i := range app.Commands {
		setFlagsAndBeforeOnSubcommands(&app.Commands[i], defaultFlags, beforeRun)
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatalf("Failed to run app with %s: %v", os.Args, err)
	}
}

func setFlagsAndBeforeOnSubcommands(command *cli.Command, defaultFlags []cli.Flag, before cli.BeforeFunc) {
	command.Flags = append(command.Flags, defaultFlags...)
	command.Before = before
	for i := range command.Subcommands {
		setFlagsAndBeforeOnSubcommands(&command.Subcommands[i], defaultFlags, before)
	}
}

func beforeRun(ctx *cli.Context) error {
	runWebProfiler(ctx)
	if err := establishLogger(ctx); err != nil {
		return err
	}
	return nil
}

func runWebProfiler(ctx *cli.Context) {
	if ctx.IsSet("net-pprof-addr") {
		go http.ListenAndServe(ctx.String("net-pprof-addr"), nil)
	}
}

func establishLogger(ctx *cli.Context) error {
	cfg := zap.NewDevelopmentConfig()
	if !ctx.IsSet("debug") {
		cfg.Level.SetLevel(zap.InfoLevel)
	}
	logger, err := cfg.Build(zap.WithCaller(false))
	if err != nil {
		return err
	}
	setting.Logger = logger.Sugar()
	return nil
}
