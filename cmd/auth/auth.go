package auth

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	_ "net/http/pprof"

	"github.com/gorilla/mux"
	"github.com/urfave/cli"

	"gitlab.com/golang-team-10/golang-mts-teta/cmd/base"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/auth"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/jwt"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/setting"
)

var Cmd = cli.Command{
	Name:        "auth",
	Action:      run,
	Usage:       "authentication server",
	Description: "authentication server",
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:      "hmac-secret",
			Usage:     "filename with hmac-secret for jwt",
			TakesFile: true,
			EnvVar:    "HMAC_SECRET",
		},
		cli.StringFlag{
			Name:      "auth-data",
			Usage:     "filename with authentication data",
			Required:  true,
			TakesFile: true,
			EnvVar:    "AUTH_DATA",
		},
	},
}

func run(ctx *cli.Context) error {
	if err := establishHmacSecret(ctx.String("hmac-secret")); err != nil {
		return err
	}
	authData, err := readAuthData(ctx.String("auth-data"))
	if err != nil {
		return err
	}
	s := http.Server{
		Addr:         ctx.String("addr"),
		Handler:      Router(authData),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}
	return s.ListenAndServe()
}

func establishHmacSecret(name string) error {
	if name == "" {
		return nil
	}
	f, err := os.Open(name)
	if err != nil {
		return err
	}
	defer f.Close()
	setting.HmacSecret, err = io.ReadAll(f)
	if err != nil {
		return err
	}
	return nil
}

func readAuthData(name string) (*auth.AuthData, error) {
	f, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return auth.NewAuthData(f)
}

func Router(authData *auth.AuthData) *mux.Router {
	r := mux.NewRouter()
	r.Path("/login").HandlerFunc(handleLogin(authData))
	r.Path("/logout").HandlerFunc(handleLogout)

	p := mux.NewRouter()
	p.Use(jwt.Middleware)
	p.Path("/i").HandlerFunc(handle_i)
	p.Path("/me").HandlerFunc(handle_i)
	p.NotFoundHandler = r

	return base.Router(p)
}

func handle_i(w http.ResponseWriter, r *http.Request) {
	resp := struct {
		Username string `json:"username"`
	}{
		Username: jwt.GetUsername(r),
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

func handleLogin(authData *auth.AuthData) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			w.Header().Add("WWW-Authenticate", `Basic realm=”golang-mts-teta”`)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if err := authData.Validate(username, password); err != nil {
			w.Header().Add("WWW-Authenticate", `Basic realm=”golang-mts-teta”`)
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(w, err)
			return
		}
		if err := jwt.AddTokens(w, username); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			setting.Logger.Error(err)
			return
		}
		redirect(w, r)
	}
}

func handleLogout(w http.ResponseWriter, r *http.Request) {
	jwt.ClearTokens(w)
	redirect(w, r)
}

func redirect(w http.ResponseWriter, r *http.Request) {
	if url := r.URL.Query().Get("redirect_uri"); url != "" {
		http.Redirect(w, r, url, http.StatusFound)
		return
	}
	w.WriteHeader(http.StatusOK)
}
