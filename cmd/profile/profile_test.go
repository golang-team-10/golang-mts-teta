package profile

import (
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang-jwt/jwt"

	"gitlab.com/golang-team-10/golang-mts-teta/cmd/base"
	h2 "gitlab.com/golang-team-10/golang-mts-teta/internal/http"
	"gitlab.com/golang-team-10/golang-mts-teta/internal/setting"
)

func init() {
	base.StaticDir = "../../static"
	setting.HmacSecret = []byte("123456")
	jwt.TimeFunc = func() time.Time {
		return time.Date(2021, 10, 8, 17, 2, 37, 0, time.UTC)
	}
}

func TestProfile(t *testing.T) {
	h2.RunTest(t, "prof", httptest.NewServer(Router()), "profile_test.yaml")
}
