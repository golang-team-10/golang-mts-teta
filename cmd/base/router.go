package base

import (
	"net/http"

	"github.com/gorilla/mux"
)

var StaticDir string = "static"

func Router(next *mux.Router) (r *mux.Router) {
	r = mux.NewRouter()
	r.PathPrefix("/static/").
		Methods(http.MethodGet).
		Handler(http.StripPrefix("/",
			http.FileServer(
				http.Dir(StaticDir))))
	r.Path("/favicon.ico").
		Methods(http.MethodGet).
		Handler(http.StripPrefix("/",
			http.FileServer(
				http.Dir(StaticDir))))
	r.Path("/").
		Methods(http.MethodGet).
		Handler(http.StripPrefix("/",
			http.FileServer(
				http.Dir(StaticDir))))
	r.NotFoundHandler = next
	return
}
